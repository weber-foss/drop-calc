function InputNumber(field, precision) {
    this.field = field;
    this.precision = precision || 2;
    if(typeof precision !== "undefined") { this.precision = precision; }
    this.setValue = function(value) {
        this.value = value;
        this.field.value = parseFloat(value).toFixed(this.precision);
    };
    this.setValue(field.value);
    this.getValue = function() {
        return this.value;
    };
}

function InputNumberSlider(field, slider, precision) {
    this.number = new InputNumber(field, precision);
    this.slider = slider;
    this.setValue = function(value) {
        this.slider.value = value;
        this.number.setValue(value);
    };
    this.getValue = function() {
        return this.number.getValue();
    };
    var _this = this;
    this.slider.addEventListener("input", function() {
        _this.number.field.value = this.value;
        _this.number.field.dispatchEvent(new Event('input'));
    });
}

/* ISO 3 bar <-> 10 bar */
factor = Math.sqrt(10 / 3);

//var nozzlePreset = document.getElementById("nozzle-preset");
var nozzleCount = document.getElementById("nozzle-count");
var nozzleCountSlider = document.getElementById("nozzle-count-slider");
var nozzleTypePreset = document.getElementById("nozzle-type-preset");
var nozzleTypeButton = document.getElementById("nozzle-type-button");
var nozzleTypeButtonLabel = document.getElementById("nozzle-type-button-label");
var nozzleType = document.getElementById("nozzle-type");
var refFlow = document.getElementById("ref-flow");

var upper = document.getElementById("upper");
var Q = document.getElementById("liters-minute");
var lower = document.getElementById("lower");

var QSlider =  document.getElementById("liters-minute-slider");

var calcLiters = document.getElementById("calc-liters");
var calcSpeed = document.getElementById("calc-speed");
var calcLitersArea = document.getElementById("calc-liters-area");

var nozzleTypeMatch = document.getElementById("nozzle-type-match");

var pressureSlider = document.getElementById("pressure-slider");
var pressure = document.getElementById("pressure");
var speedSlider = document.getElementById("speed-slider");
var speed = document.getElementById("speed");
var rowWidthSlider = document.getElementById("row-width-slider");
var rowWidth = document.getElementById("row-width");
var litersAreaSlider = document.getElementById("liters-area-slider");
var litersArea = document.getElementById("liters-area");

var inputNumberNozzleCount = new InputNumberSlider(nozzleCount, nozzleCountSlider, 0);
var inputNumberPressure = new InputNumberSlider(pressure, pressureSlider, 1);
var inputNumberRowWidth = new InputNumberSlider(rowWidth, rowWidthSlider, 1);
var inputNumberSpeed = new InputNumberSlider(speed, speedSlider, 1);
var inputNumberLitersArea = new InputNumberSlider(litersArea, litersAreaSlider, 0);
var inputNumberQ = new InputNumberSlider(Q, QSlider, 1);

var inputNumberRefFlow = new InputNumber(refFlow, 1);
var inputNumberNozzleType = new InputNumber(nozzleType);
//var inputNumberQ = new InputNumber(Q, 1);

// (Zinkgelb), (Verkehrsgrün), (Reinorange), (Hellrosa), (Olivgrün), (Blaulila)
var nozzleColors = ["f6a950", "008351",  "e25303", "d8a0a6", "50533c", "76689a"];
var nozzleIds = ["nozzle-1","nozzle-2","nozzle-3","nozzle-4","nozzle-5","nozzle-6"];


function lumina(c) {
    var rgb = parseInt(c, 16);
    var r = (rgb >> 16) & 0xff;
    var g = (rgb >>  8) & 0xff; 
    var b = (rgb >>  0) & 0xff;
    
    /* per ITU-R BT.709 */
    var luma = 0.2126 * r + 0.7152 * g + 0.0722 * b;
    
    return luma < 200;
}

function nozzleChange() {
    inputNumberNozzleType.setValue(this.value * factor);
    referenceFlow();
    nozzleTypeButton.style.backgroundColor = document.querySelector('label[for=' + this.id + ']').style.color;
    nozzleTypeButtonLabel.innerHTML = document.querySelector('label[for=' + this.id + ']').innerHTML;
    window.location.href = '#';
}

/* calculations */

/* reference flow 10 bar */
function referenceFlow() {
    inputNumberRefFlow.setValue(inputNumberNozzleCount.getValue() * inputNumberNozzleType.getValue());
    upperBox();
}

function qFromUpper() {
    inputNumberQ.setValue(inputNumberRefFlow.getValue() * Math.sqrt(inputNumberPressure.getValue() / 10));
    lowerFromQ();
}

function qFromLower() {
    inputNumberQ.setValue(inputNumberSpeed.getValue() * inputNumberRowWidth.getValue() * inputNumberLitersArea.getValue() / 600);
    upperFromQ();
}

function lowerFromQ() {
    if(calcSpeed.checked) {
        inputNumberSpeed.setValue(inputNumberQ.getValue() / ( inputNumberLitersArea.getValue() * inputNumberRowWidth.getValue() ) * 600);
    } else if(calcLitersArea.checked) {
        inputNumberLitersArea.setValue(inputNumberQ.getValue() / ( inputNumberSpeed.getValue() * inputNumberRowWidth.getValue() ) * 600);
    }
}

function upperFromQ() {
    inputNumberPressure.setValue(Math.pow( inputNumberQ.getValue() / inputNumberRefFlow.getValue(), 2 ) * 10);
}

function lowerBox() {
    if(calcLiters.checked) {
        qFromLower();
    } else {
        lowerFromQ();
    }
}

function upperBox() {
    if(calcLiters.checked) {
        qFromUpper();
    } else {
        upperFromQ();
    }
}

/* calculation modes */

function calcModeLower() {
    speedSlider.disabled = speed.disabled = (!calcLiters.checked && calcSpeed.checked);
    litersAreaSlider.disabled = litersArea.disabled = (!calcLiters.checked && calcLitersArea.checked);
}

/* calculation options listeners */

calcLiters.addEventListener("change", function() {
    pressureSlider.disabled = pressure.disabled = !this.checked;
    calcModeLower();
});

calcSpeed.addEventListener("change", function() {
    calcModeLower();
});

calcLitersArea.addEventListener("change", function() {
    calcModeLower();
});

/* preset listeners */

/*
nozzlePreset.addEventListener("input", function() {
    inputNumberNozzleCount.setValue(this.value);
    nozzleCount.dispatchEvent(new Event('input'));
});
nozzleCount.addEventListener("input", function() {
    nozzlePreset.value = nozzleCount.value;
});
*/

/* input event not working in edge */
/*
nozzleTypePreset.addEventListener("change", function() {
    inputNumberNozzleType.setValue(this.value * factor);
    referenceFlow();
    nozzleTypePresetColor();
});
*/
nozzleIds.forEach(function(nozzleId) {
    document.getElementById(nozzleId).addEventListener("click", nozzleChange);
});

/* not used */
function nozzleTypePresetColor() {
    for (var i = 0; i < nozzleTypePreset.options.length; i++) {
        var oval = nozzleTypePreset.options[i].value;
        /* from ISO 3 bars to 10 bars */
        var ovalDiff = Math.abs( 1 - ( inputNumberNozzleType.getValue() / ( oval * factor ) ) );
        /* 5% deviation ! */
        if(ovalDiff <= 0.05) {
            nozzleTypePreset.value = oval;
            //nozzleTypePreset.style.borderColor = '#' + nozzleColors[i];
            nozzleTypePreset.style.backgroundColor = nozzleTypePreset.options[i].style.backgroundColor;
            //nozzleTypePreset.style.color = nozzleTypePreset.options[i].style.color;
            return;
        } else {
            nozzleTypePreset.value = '';
            //nozzleTypePreset.style.borderColor = '#ccc';
            nozzleTypePreset.style.backgroundColor = 'transparent';
        }
    }
};

/* calculation / entity listeners */

nozzleCount.addEventListener("input", function() {
    console.log('ui');
    inputNumberNozzleCount.setValue(nozzleCount.value);
    referenceFlow();
});

nozzleType.addEventListener("input", function() {
    inputNumberNozzleType.setValue(nozzleType.value);
    referenceFlow();
    nozzleTypePresetColor();
});

pressure.addEventListener("input", function() {
    inputNumberPressure.setValue(pressure.value);
    qFromUpper();
});

speed.addEventListener("input", function() {
    inputNumberSpeed.setValue(speed.value);
    lowerBox();
});

rowWidth.addEventListener("input", function() {
    inputNumberRowWidth.setValue(rowWidth.value);
    lowerBox();
});

litersArea.addEventListener("input", function() {
    inputNumberLitersArea.setValue(litersArea.value);
    lowerBox();
});

Q.addEventListener("input", function() {
    inputNumberQ.setValue(Q.value);
    lowerFromQ();
    upperFromQ();
});

/* init */
/*
nozzleTypePreset.dispatchEvent(new Event('change'));
*/
